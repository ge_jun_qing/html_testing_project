var obj = null;
// jquery页面初始化操作
$(document).ready(function () {
    $(".bill-action").click(function (e){
        $("#dialog").dialog();
        obj = $(e.target);
        $("#dialog-body").empty();

        var data = obj.parent().parent().find("input[class='bill_info']").val();
        var all;
        if(data !== '')
        {
            all = JSON.parse(data);
        }
        else
        {
            all = IndexUtil.initData();
        }
        // 根据数据拼接html
        $("#dialog-body").append(IndexUtil.initHtml(all));
    });
    $('#dialog').dialog('close');
});

IndexUtil = {
    initData:function () {
        var all = {};
        all.section1 = [{'billType':'1','bili':''}];
        all.section2 = [{'billType':'1','bili':'','minAmt':'','maxAmt':''}];
        all.section3 = [{'billType':'1','bili':'','minAmt':'','maxAmt':'','sectionMinAmt':''}];

        return all;
    },
    getSelected:function (i,j) {
        if(i === j)
        {
            return "selected";
        }
        return "";
    },
    initHtml:function (all) {
        var html = "<div>";
        // 第一段
        html = html + "<div id='section1'>";
        for (let i = 0; i < all.section1.length; i++) {
            html = html + "<div class='text-align-class'><select name='billType'>" +
                "<option value='1' "+this.getSelected(all.section1[i].billType,'1')+">按笔计费</option>" +
                "<option value='2' "+this.getSelected(all.section1[i].billType,'2')+">按金额计费</option> " +
                "</select>&nbsp;&nbsp;<input type='text' name='bili' value='"+all.section1[i].bili+"'/><p>%</p></div>";
        }
        html = html + "</div>";
        // 第二段
        html = html + "<div id='section2'>";
        for (let i = 0; i < all.section2.length; i++) {
            html = html + "<div class='text-align-class'><select name='billType'><option value='1' "+this.getSelected(all.section2[i].billType,'1')+">按笔计费</option><option value='2' "+this.getSelected(all.section2[i].billType,'2')+">按金额计费</option> </select>&nbsp;&nbsp;<input type='text' name='bili' value='"+all.section2[i].bili+"'/>&nbsp;&nbsp;<p>% 保底:</p><input type='text' name='minAmt' value='"+all.section2[i].minAmt+"'/>&nbsp;&nbsp;<p> 封顶:</p><input type='text' name='maxAmt' value='"+all.section2[i].maxAmt+"'/></div>";
        }
        html = html + "</div>";
        // 第三段
        html = html + "<div id='section3'>";
        for (let i = 0; i < all.section3.length; i++) {
            html = html + "<div class='text-align-class'><p> 分段下限(含):</p><input type='text' name='sectionMinAmt' value='"+all.section3[i].sectionMinAmt+"'/> <p> 元 </p>&nbsp;&nbsp;<select name='billType'><option value='1' "+this.getSelected(all.section3[i].billType,'1')+">按笔计费</option><option value='2' "+this.getSelected(all.section3[i].billType,'2')+">按金额计费</option> </select>&nbsp;&nbsp;<input type='text' name='bili' value='"+all.section3[i].bili+"'/>&nbsp;&nbsp;<p>% 保底:</p><input type='text' name='minAmt' value='"+all.section3[i].minAmt+"'/>&nbsp;&nbsp;<p> 封顶:</p><input type='text' name='maxAmt' value='"+all.section3[i].maxAmt+"'/></div>";
        }
        html = html + "</div>";
        // }
        // else
        // {
        //     // 第一段
        //     html = html + "<div id='section1'>";
        //     html = html + "<div class='text-align-class'><select name='billType'><option value='1'>按笔计费</option><option value='2'>按金额计费</option> </select><input type='text' name='bili'/><p>%</p></div>";
        //     html = html + "</div>";
        //     // 第二段
        //     html = html + "<div id='section2'>";
        //     html = html + "<div class='text-align-class'><select name='billType'><option value='1'>按笔计费</option><option value='2'>按金额计费</option> </select><input type='text' name='bili'/><p>% 保底:</p><input type='text' name='minAmt'/><p> 封顶:</p><input type='text' name='maxAmt'/></div>";
        //     html = html + "</div>";
        //     // 第三段
        //     html = html + "<div id='section3'>";
        //     html = html + "<div class='text-align-class'><p> 分段下限(含):</p><input type='text' name='sectionMinAmt'/> <p> 元 </p><select name='billType'><option value='1'>按笔计费</option><option value='2'>按金额计费</option> </select><input type='text' name='bili'/><p>% 保底:</p><input type='text' name='minAmt'/><p> 封顶:</p><input type='text' name='maxAmt'/></div>";
        //     html = html + "</div>";
        // }
        // 结束
        html = html + "</div>"

        return html;
    },
    saveJsonData:function () {
        var section1Array = [];
        $.each($("#section1").find("div"),function (i,e)
        {
            var section1obj = {};
            section1obj.billType = $(e).find("select[name='billType'] option:selected").val();
            section1obj.bili = $(e).find("input[name='bili']").val();
            // 调试
            console.log(section1obj);
            section1Array.push(section1obj);
        });
        var section2Array = [];
        $.each($("#section2").find("div"),function (i,e)
        {
            var section2obj = {};
            section2obj.billType = $(e).find("select[name='billType'] option:selected").val();
            section2obj.bili = $(e).find("input[name='bili']").val();
            section2obj.minAmt = $(e).find("input[name='minAmt']").val();
            section2obj.maxAmt = $(e).find("input[name='maxAmt']").val();
            // 调试
            console.log(section2obj);
            section2Array.push(section2obj);
        })
        var section3Array = [];
        $.each($("#section3").find("div"),function (i,e)
        {
            var section3obj = {};
            section3obj.sectionMinAmt = $(e).find("input[name='sectionMinAmt']").val();
            section3obj.billType = $(e).find("select[name='billType'] option:selected").val();
            section3obj.bili = $(e).find("input[name='bili']").val();
            section3obj.minAmt = $(e).find("input[name='minAmt']").val();
            section3obj.maxAmt = $(e).find("input[name='maxAmt']").val();
            // 调试
            console.log(section3obj);
            section3Array.push(section3obj);
        })
        var all = {};
        all.section1 = section1Array;
        all.section2 = section2Array;
        all.section3 = section3Array;
        //保存结果
        obj.parent().parent().find("input[class='bill_info']").val(JSON.stringify(all));
        $('#dialog').dialog('close');
    }
}